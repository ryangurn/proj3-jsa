# proj3-JSA
Vocabulary anagrams game for primary school English language learners (ELL)

## Setup
1. Running `.\run.sh` will delete all previous images and build a new one. **You must ensure that the credentials.ini file is in the `vocab` folder**
2. Run the newly created container that was built.

## [Testing](docs/tests.md)
1. `cd <project dir>/vocab`
2. `pip install nose` (if you need to install it)
3. `nosetests`

## Features
1. AJAX front end that checks for match
2. Backend logic to handle the AJAX requests
3. Working JQuery logic to handle the http requests that are needed to check for matches and update the view.
4. JQuery that allows us to highlight the completed word from the list (Extra Credit)
5. Highlight specific characters in the word jumble as you are typing (Extra Credit)
6. Highlight only one character at a time in the list 

## Additional Documentation
- [Gameplay](docs/game.md)
- [Configurations](docs/config.md)