# Config

## `Credentials.ini` **_Required_**
- Port (PORT): this is the port that the docker will internall bind to. You can change the outgoing port via docker, please refer to their documentation.
- Debug (DEBUG): this allows you to see debug information (by default: True) this is so that you can see the server running
- Secret Key (secret_key): this is the required secret key for the flask system to operate, for any questions refer yourself to their documentation (good luck).
- Success At Count (success_at_count): this is the number of words that you need to find in order to complete the game
- Vocab List (vocab): this is the list of vocab that we will use to match against.
