# Gameplay

1. First what you will see when going to `http://localhost:5000` is the following.
![Step 1](img/step1.png)
2. What you will see from here is as you type characters the jumbled letters will highlight if they match the letters in the input box (one for one). ![Step 2](img/step2.png)
3. Following that you will notice that there is a number of words that you need to find to finish the game. (In this case its 5). Once you complete them all you will briefly see the following telling you what you found and letting you know that you will be redirected!  ![Step 3](img/step3.png)
4. After that you will see the success page! ![Step 4](img/step4.png)