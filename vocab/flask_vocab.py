"""
Flask web site with vocabulary matching game
(identify vocabulary words that can be made 
from a scrambled string)
"""
import os

import flask
import logging

# Our own modules
from letterbag import LetterBag
from vocab import Vocab
from jumble import jumbled
import config

###
# Globals
###
app = flask.Flask(__name__)

CONFIG = config.configuration()
try:
    app.secret_key = CONFIG.SECRET_KEY  # Should allow using session variables
except AttributeError:
    app.secret_key = os.urandom(24)
#
# One shared 'Vocab' object, read-only after initialization,
# shared by all threads and instances.  Otherwise we would have to
# store it in the browser and transmit it on each request/response cycle,
# or else read it from the file on each request/responce cycle,
# neither of which would be suitable for responding keystroke by keystroke.
try:
    WORDS = Vocab(CONFIG.VOCAB) # grab from the config if provided
except AttributeError:
    WORDS = Vocab("data/third_grade.txt") # default to provided information if not

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    """The main page of the application"""
    flask.g.vocab = WORDS.as_list()
    try:
        flask.session["target_count"] = min(
            len(flask.g.vocab), CONFIG.SUCCESS_AT_COUNT)
        app.logger.info("Success At Count = {}".format(CONFIG.SUCCESS_AT_COUNT))
    except AttributeError:
        flask.session["target_count"] = min(
            len(flask.g.vocab), 3)
        app.logger.info("Success At Count = 3 // Please add a credentials.ini")
    flask.session["jumble"] = jumbled(
        flask.g.vocab, flask.session["target_count"])
    flask.session["jumbleHTML"] = ""
    for c in flask.session["jumble"]:
        flask.session["jumbleHTML"] += "<span class='letter-"+c+"'>"+c+"</span>"
    flask.session["matches"] = []
    app.logger.debug("Session variables have been set")
    assert flask.session["matches"] == []
    assert flask.session["target_count"] > 0
    app.logger.debug("At least one seems to be set correctly")
    return flask.render_template('vocab.html')

@app.route("/success")
def success():
    return flask.render_template('success.html')

#######################
# Form handler.
# CIS 322 note:
#   You'll need to change this to a
#   a JSON request handler
#######################


@app.route("/_check", methods=["POST"])
def check():
    """
    User has submitted the form with a word ('attempt')
    that should be formed from the jumble and on the
    vocabulary list.  We respond depending on whether
    the word is on the vocab list (therefore correctly spelled),
    made only from the jumble letters, and not a word they
    already found.
    """
    app.logger.debug("Entering check")

    # The data we need, from form and from cookie
    text = flask.request.form["attempt"]
    jumble = flask.request.form["jumble"]
    matches = flask.session.get("matches", [])  # Default to empty list

    # Is it good?
    in_jumble = LetterBag(jumble).contains(text)
    matched = WORDS.has(text)

    # Respond appropriately
    if matched and in_jumble and not (text in matches):
        # Cool, they found a new word
        matches.append(text)
        flask.session["matches"] = matches
    elif text in matches:
        flask.flash("You already found {}".format(text))
    elif not matched:
        flask.flash("{} isn't in the list of words".format(text))
    elif not in_jumble:
        flask.flash(
            '"{}" can\'t be made from the letters {}'.format(text, jumble))
    else:
        app.logger.debug("This case shouldn't happen!")
        assert False  # Raises AssertionError

    # Choose page:  Solved enough, or keep going?
    if len(matches) > 0:
        app.logger.debug("Matches Found")
        completed = False
        app.logger.debug("Determining if target count was reached")
        if len(matches) == flask.session["target_count"]:
            app.logger.debug("Target count reached")
            completed = True
        else:
            app.logger.debug("Target count not reached")

        json = flask.jsonify(matches=matches, text=text, required=flask.session["target_count"], completed_amt=len(matches), completed=completed)
        return json
    else:
        app.logger.debug("No Matches Found")
        json = flask.jsonify(matches=None, attempt=text)
        return json

###################
#   Error handlers
###################


@app.errorhandler(404)
def error_404(e):
    app.logger.warning("++ 404 error: {}".format(e))
    return flask.render_template('404.html'), 404


@app.errorhandler(500)
def error_500(e):
    app.logger.warning("++ 500 error: {}".format(e))
    return flask.render_template('500.html'), 500


@app.errorhandler(403)
def error_403(e):
    app.logger.warning("++ 403 error: {}".format(e))
    return flask.render_template('403.html'), 403


####

if __name__ == "__main__":
    if CONFIG.DEBUG:
        app.debug = True
        app.logger.setLevel(logging.DEBUG)
        app.logger.info(
            "Opening for global access on port {}".format(CONFIG.PORT))
        app.run(port=CONFIG.PORT, host="0.0.0.0") #grab from config if possible
    else:
        app.debug = False
        app.logger.setLevel(logging.INFO)
        app.logger.info(
            "Opening for global access on port {}".format(CONFIG.PORT))
        app.run(port=CONFIG.PORT, host="0.0.0.0") #grab from config if possible